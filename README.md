# RobAIR-TP-config

Fichiers de configuration pour les PC, à usage des projets de robotique autour de RobAIR https://github.com/fabMSTICLig/RobAIR dans le cadre des cours d'Olivier Aycard https://lig-membres.imag.fr/aycard/html/Enseignement/M1/Robotics/

## .bashrc
À placer à la racine de chaque home.

## switch_config.sh
À placer à la racine de chaque home, avec droit d'exécution.

## 01-RobAIR-motd
À placer dans le dossier /etc/update-motd.d/ avec droit d'exécution.

## rviz_config_laser.rviz
À placer dans le dossier Document de chaque home. 

