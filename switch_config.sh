#!/bin/sh

CATKIN_DEVEL_DIR="${HOME}/catkin_ws/devel"

if [ "$1" = "robair" ] ; then
	nmcli connection down "default Network" 2>/dev/null 1>&2 &
	nmcli connection up "Robair Network" 2>/dev/null 1>&2 &
elif [ "$1" = "local" ] ; then
	nmcli connection down "Robair Network" 2>/dev/null 1>&2 &
	nmcli connection up "default Network" 2>/dev/null 1>&2 &
else
	echo "\033[91mError\033[0m: need one argument\n\tUsage:\t$0 robair|local\n"
	exit 1
fi
echo "$1" > /.robairConf
echo "config $1\033[92m OK\033[0m, you need to \033[92mopen new terminal\033[0m to apply config"


